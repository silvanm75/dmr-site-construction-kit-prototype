<?php
/**
 * @author Silvan
 */

namespace Sitekit;


class Page extends Module\AbstractModule {

	protected $content;

	public function setContent( $value ) {
		$this->content = $value;
	}

	public function __toString() {
		ob_start();
		include('views/page.phtml');
		return ob_get_clean();
	}
}