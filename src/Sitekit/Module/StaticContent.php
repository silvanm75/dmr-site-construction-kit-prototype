<?php
/**
 * @author Silvan
 */

namespace Sitekit\Module;

class StaticContent extends AbstractModule {

	protected $content;
	
	public function setContent($value) {
		$this->content = $value;
	}

}