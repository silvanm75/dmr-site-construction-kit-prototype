<?php
/**
 * @author Silvan
 */

namespace Sitekit\Module;

/**
 * Class ContentCollection
 * Allows adding mutiple modules to one slot
 * @package Sitekit\Module
 */
class ContentCollection extends AbstractModule {

	/** @var  AbstractModule[] */
	protected $contentCollection;

	public function setContent( Array $value ) {
		$this->contentCollection = $value;
	}

	public function __toString() {
		$output = '';
		foreach ( $this->contentCollection as $content ) {
			$output .= $content;
		}

		return $output;
	}

}