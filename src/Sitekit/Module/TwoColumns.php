<?php
/**
 * @author Silvan
 */

namespace Sitekit\Module;

class TwoColumns extends AbstractModule {

	/** @var  AbstractModule */
	protected $column1;

	/** @var  AbstractModule */
	protected $column2;

	public function setColumn1(AbstractModule $module) {
		$this->column1 = $module;
	}

	public function setColumn2(AbstractModule $module ) {
		$this->column2 = $module;
	}

}