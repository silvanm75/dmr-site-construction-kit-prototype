<?php
/**
 * @author Silvan
 */

namespace Sitekit\Module;

abstract class AbstractModule {

	protected $config;

	public function __construct($config)
	{
		foreach ( $config as $attributeName => $attributeValue ) {
			$methodName = 'set' . ucfirst( $attributeName );

			if (method_exists($this, $methodName)) {
				$this->$methodName( $attributeValue );
			} else {
				$this->$attributeName = $attributeValue;
			}
		}
	}

	/**
	 * @return string
	 */
	public function __toString() {
		ob_start();
		$reflect = new \ReflectionClass($this);
		include('views/module/' . lcfirst($reflect->getShortName()) . '.phtml');
		return ob_get_clean();
	}

}