<?php

use Sitekit\Module\StaticContent;
use Sitekit\Module\TwoColumns;
use Sitekit\Module\ContentCollection;
use Sitekit\Page;

include "vendor/autoload.php";

// We create the page
// This structure here can be easily serialized (e.g. don't instantiate the classes right now)
$page = new Page(
	[
		'title'   => 'Hello, this is a proof of concept of the DMR Sitekit',
		'content' => new ContentCollection ( [
			'content' => [
			new TwoColumns( [
				'column1' =>
					new StaticContent( [
						'content' => 'Content for column 1'
					] ),
				'column2' =>
					new TwoColumns( [
						'column1' =>
							new StaticContent( [
								'content' => 'Content for column 2.1'
							] )
						,
						'column2' =>
							new StaticContent( [
								'content' => 'Content for column 2.2'
							] )
					] ),
			] ),
			new StaticContent([
			'content' => '<p>Something in the footer</p>'])
		]] )
	] );

// The page is being rendered by using the __toString() methods
echo $page;;
